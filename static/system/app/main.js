const appsKey = "apps";
const appsPrefix = "/app";

function calcToDelete(oldInfo, newInfo = null) {
  let files = new Set();
  let pages = new Set();

  files.add(oldInfo.file_icon_id);

  files.add(oldInfo.main_page.file_id);
  pages.add(oldInfo.main_page.path);

  oldInfo.resources.forEach((page) => {
    files.add(page.file_id);
    pages.add(page.path);
  });

  if (newInfo) {
    files.delete(newInfo.file_icon_id);

    files.delete(newInfo.main_page.file_id);
    pages.delete(newInfo.main_page.path);

    newInfo.resources.forEach((page) => {
      files.delete(page.file_id);
      pages.delete(page.path);
    });
  }

  let fileTmp = [];
  files.forEach((e) => fileTmp.push(e));

  let pageTmp = [];
  pages.forEach((e) => pageTmp.push(e));

  return {
    files: fileTmp,
    pages: pageTmp,
  };
}

async function cleanOldProject(oldArtifactID, newInfo) {
  let raw = await APIRequest("/api/artifact/get", {
    id: oldArtifactID,
  });
  let oldInfo = JSON.parse(raw.value);

  let diff = calcToDelete(oldInfo, newInfo);

  for (let index = 0; index < diff.pages.length; index++) {
    await APIRequest("/api/web-page/delete", {
      path: diff.pages[index],
    });
  }

  for (let index = 0; index < diff.files.length; index++) {
    await APIRequest("/api/file/delete", { id: diff.files[index] });
  }

  await APIRequest("/api/artifact/delete", { id: oldArtifactID });
}

async function uploadAppInfo(appInfo) {
  let response = await APIRequest("/api/artifact/create", {
    value: JSON.stringify(appInfo, null, "  "),
  });
  return response.id;
}

async function syncAppState(appInfo, newInfoArtifactID) {
  let state = await getAppsState();
  let oldAppState = null;

  if (state.apps[appInfo.code]) {
    oldAppState = state.apps[appInfo.code];
    console.log(`старое приложение "${appInfo.code}" найдено`);
  }

  state.apps[appInfo.code] = {
    id: newInfoArtifactID,
    icon_id: appInfo.file_icon_id,
    name: appInfo.name,
    page_link: appInfo.main_page.path,
    version: appInfo.version,
  };

  await APIRequest("/api/state/set", {
    key: appsKey,
    value: JSON.stringify(state, null, "  "),
  });

  if (oldAppState) {
    await cleanOldProject(oldAppState.id, appInfo);
  }
}

async function getAppsState() {
  let state = null;

  try {
    let oldInfo = await APIRequest("/api/state/get", { key: appsKey });
    state = JSON.parse(oldInfo.value);
  } catch (err) {
    if (!err.not_found) {
      throw err;
    }

    state = {
      apps: {},
    };
  }

  return state;
}

async function getAppData(code) {
  let state = await getAppsState();

  // Нет такого приложения
  if (!state.apps[code]) {
    return null;
  }

  let appState = state.apps[code];

  let raw = await APIRequest("/api/artifact/get", {
    id: appState.id,
  });
  let appInfo = JSON.parse(raw.value);

  return {
    state: appState,
    artifact: appInfo,
  };
}
