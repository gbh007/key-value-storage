async function exportApplication(code) {
  async function getFile(file_id, page_path = null) {
    let file = await APIRequest("/api/file/get", { id: file_id });

    let data = {
      body: file.body,
      name: file.filename,
      mime: file.mime,
    };

    if (page_path) {
      data.path = page_path;
    }

    return data;
  }

  let data = await getAppData(code);

  // Нет такого приложения
  if (!data) {
    return;
  }

  let appState = data.state;
  let appInfo = data.artifact;

  let dumpData = {
    code: code,
    name: appState.name,
    version: appState.version || "",
  };

  dumpData.icon = await getFile(appState.icon_id);
  dumpData.main_page = await getFile(
    appInfo.main_page.file_id,
    appInfo.main_page.path
  );
  dumpData.resources = [];

  if (appInfo.resources) {
    for (let index = 0; index < appInfo.resources.length; index++) {
      let resource = appInfo.resources[index];

      let fileDump = await getFile(resource.file_id, resource.path);

      dumpData.resources.push(fileDump);
    }
  }

  let binaryData = new Blob([JSON.stringify(dumpData, null, "  ")], {
    type: "application/json",
  });

  const url = window.URL.createObjectURL(binaryData);
  const a = document.createElement("a");

  a.style.display = "none";
  a.href = url;
  a.download = `${dumpData.code}_${dumpData.version}.json`;

  document.body.appendChild(a);
  a.click();

  window.URL.revokeObjectURL(url);
  a.remove();
}
