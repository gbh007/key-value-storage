window.addEventListener("load", function () {
  let node = document.createElement("div");
  node.id = "main-header";
  node.innerHTML = `
        <h1>Key Value Storage</h1>
        <div class="std-row">
            <a class="std-fake-button" href="/">Главная</a>
            <a class="std-fake-button" href="/system/artifact/list.html">Артефакты</a>
            <a class="std-fake-button" href="/system/state/list.html">Состояния</a>
            <a class="std-fake-button" href="/system/file/list.html">Файлы</a>
            <a class="std-fake-button" href="/system/web-page/list.html">Веб страницы</a>
        </div>
    `;
  document.body.insertBefore(node, document.body.firstChild);
});
