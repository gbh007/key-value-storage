function escapeHTML(v) {
  let node = document.createElement("span");
  node.innerText = v;
  return node.innerHTML;
}

function insertPagination(node, pageInfo) {
  node.innerHTML = `
<div class="pagination">
            ${pageInfo.pretty_pages
              .map(
                (p) =>
                  `<div class="pagination-page" ${
                    p < 1
                      ? ""
                      : `onclick="refresh(${p})" tp="${
                          p == pageInfo.current ? "current" : "page"
                        }`
                  }">${p < 1 ? "..." : escapeHTML(p)}</div>`
              )
              .join("\n")}
            <span>всего элементов <b>${pageInfo.items}</b></span>
</div>
            `;
}

function encodeFile(fileRaw) {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();

    reader.onload = function () {
      let fileBody = reader.result;
      const sep = "base64,";
      base64prefix = fileBody.slice(0, fileBody.indexOf(sep) + sep.length);
      fileBody = fileBody.slice(fileBody.indexOf(sep) + sep.length);

      resolve({
        body: fileBody,
        base64prefix: base64prefix,
        name: fileRaw.name,
        type: fileRaw.type,
      });
    };

    reader.onerror = function (error) {
      reject(error);
    };

    reader.readAsDataURL(fileRaw);
  });
}
