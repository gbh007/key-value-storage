async function APIRequest(url, data = null, method = "POST") {
  function appAlert(text) {
    console.log(text);
    alert(text);
  }

  return new Promise((resolve, reject) => {
    let requestInit = {
      method: method,
    };

    if (data != null) {
      requestInit.body = JSON.stringify(data);
    }

    fetch(url, requestInit)
      .then((response) => {
        // No content
        if (response.status == 204) {
          resolve(null);
          return;
        }

        if (response.status == 404) {
          reject({ not_found: true });
          return;
        }

        if (response.ok) {
          response
            .json()
            .then((responseJSONData) => resolve(responseJSONData))
            .catch((someError) => {
              appAlert(someError.toString());
              reject(someError.toString());
            });
        } else {
          response
            .json()
            .then((responseJSONData) => {
              appAlert(responseJSONData);
              reject(responseJSONData);
            })
            .catch(() => {
              response
                .text()
                .then((responseTextData) => {
                  appAlert(responseTextData);
                  reject(responseTextData);
                })
                .catch((someError) => {
                  appAlert(someError.toString());
                  reject(someError.toString());
                });
            });
        }
      })
      .catch((someError) => {
        appAlert(someError.toString());
        reject(someError.toString());
      });
  });
}
