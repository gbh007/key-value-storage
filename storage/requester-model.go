package storage

// RequesterArtifact - артефакт прокси-запроса
type RequesterArtifact struct {
	// Завершен ли запрос
	Done bool `json:"done"`
	// Данные запроса
	RequestData HTTPData `json:"request"`
	// Данные ответа
	ResponseData HTTPData `json:"response,omitempty"`
	// HTTP код ответа
	ResponseCode int `json:"http_code,omitempty"`
	// Данные ошибки, если произошла
	Error string `json:"error,omitempty"`
}

// HTTPData - данные для запроса/ответа
type HTTPData struct {
	// Адрес на который необходимо делать запрос
	URL string `json:"url,omitempty"`
	// Метод запроса
	Method string `json:"method,omitempty"`
	// Заголовки запроса/ответа
	Headers []Header `json:"headers,omitempty"`
	// Печеньки запроса/ответа
	Cookies []Cookie `json:"cookies,omitempty"`
	// ИД файла в который сохранен запрос/ответ (если есть),
	BodyFileID int64 `json:"file_id,omitempty"`
}

// Header - данные заголовков
type Header struct {
	// Название заголовка
	Name string `json:"name"`
	// Значения заголовков
	Values []string `json:"values"`
}

// Cookie - данные печенек
type Cookie struct {
	// Название печеньки
	Name string `json:"name"`
	// Значение печеньки
	Value string `json:"value"`
}
