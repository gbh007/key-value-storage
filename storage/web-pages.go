package storage

import (
	"context"
	"database/sql"
)

// WebPage - данные веб страницы
type WebPage struct {
	// Путь до страницы
	Path string `db:"path"`
	// ИД файла со страницей
	FileID int64 `db:"file_id"`
	// Название страницы
	Name sql.NullString `db:"name"`
	// Время создания страницы
	Created string `db:"created"`
	// Время последнего обновления страницы
	Updated sql.NullString `db:"updated"`
}

// InsertWebPage - добавляет новую веб страницу
func (storage *Storage) InsertWebPage(ctx context.Context, path string, fileID int64, name sql.NullString) error {
	_, err := storage.db.ExecContext(ctx, `INSERT INTO web_pages(path, file_id, name, created) VALUES(?, ?, ?, ?);`, path, fileID, name, tnow())
	if err != nil {
		return err
	}

	return nil
}

// UpdateWebPage - обновляет веб страницу
func (storage *Storage) UpdateWebPage(ctx context.Context, oldPath, newPath string, fileID int64, name sql.NullString) error {
	_, err := storage.db.ExecContext(ctx, `UPDATE web_pages SET file_id = ?, updated = ?, path = ?, name = ? WHERE LOWER(path) = ?;`, fileID, tnow(), newPath, name, oldPath)
	if err != nil {
		return err
	}

	return nil
}

// DeleteWebPage - удаляет веб страницу
func (storage *Storage) DeleteWebPage(ctx context.Context, path string) error {
	_, err := storage.db.ExecContext(ctx, `DELETE FROM web_pages WHERE LOWER(path) = ?;`, path)
	if err != nil {
		return err
	}

	return nil
}

// SelectWebPage - получает веб страницу по ссылке
func (storage *Storage) SelectWebPage(ctx context.Context, path string) (*WebPage, error) {
	item := new(WebPage)
	err := storage.db.GetContext(ctx, item, `SELECT * FROM web_pages WHERE LOWER(path) = ?;`, path)
	if err != nil {
		return nil, err
	}

	return item, nil
}

// SelectWebPages - получает веб страницы
func (storage *Storage) SelectWebPages(ctx context.Context, limit, offset int64, onlyNamed bool) ([]*WebPage, error) {
	list := make([]*WebPage, 0)

	query := `SELECT * FROM web_pages`

	if onlyNamed {
		query += " WHERE name IS NOT NULL"
	}

	query += " ORDER BY path"

	if limit > 0 {
		query += " LIMIT ? OFFSET ?"
	}

	query += ";"

	err := storage.db.SelectContext(ctx, &list, query, limit, offset)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// SelectWebPageCount - получает количество веб страниц
func (storage *Storage) SelectWebPageCount(ctx context.Context, onlyNamed bool) (int64, error) {
	var count int64

	query := `SELECT COUNT(*) FROM web_pages`

	if onlyNamed {
		query += " WHERE name IS NOT NULL"
	}

	query += ";"

	err := storage.db.GetContext(ctx, &count, query)
	if err != nil {
		return 0, err
	}

	return count, nil
}
