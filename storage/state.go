package storage

import (
	"context"
	"database/sql"
)

// State - данные состояний
type State struct {
	// Ключ записи
	Key string `db:"key"`
	// Значение записи
	Value string `db:"value"`
	// Время создания записи
	Created string `db:"created"`
	// Время последнего обновления записи
	Updated sql.NullString `db:"updated"`
}

// InsertState - добавляет новое состояние
func (storage *Storage) InsertState(ctx context.Context, key, value string) error {
	_, err := storage.db.ExecContext(ctx, `INSERT INTO states(key, value, created) VALUES(?, ?, ?);`, key, value, tnow())
	if err != nil {
		return err
	}

	return nil
}

// UpdateState - обновляет состояние
func (storage *Storage) UpdateState(ctx context.Context, oldKey, newKey, value string) error {
	_, err := storage.db.ExecContext(ctx, `UPDATE states SET value = ?, updated = ?, key = ? WHERE key = ?;`, value, tnow(), newKey, oldKey)
	if err != nil {
		return err
	}

	return nil
}

// DeleteState - удаляет состояние
func (storage *Storage) DeleteState(ctx context.Context, key string) error {
	_, err := storage.db.ExecContext(ctx, `DELETE FROM states WHERE key = ?;`, key)
	if err != nil {
		return err
	}

	return nil
}

// SelectState - получает состояние по ключу
func (storage *Storage) SelectState(ctx context.Context, key string) (*State, error) {
	item := new(State)
	err := storage.db.GetContext(ctx, item, `SELECT * FROM states WHERE key = ?;`, key)
	if err != nil {
		return nil, err
	}

	return item, nil
}

// SelectStates - получает состояния
func (storage *Storage) SelectStates(ctx context.Context, limit, offset int64) ([]*State, error) {
	list := make([]*State, 0)

	query := `SELECT * FROM states ORDER BY key`

	if limit > 0 {
		query += " LIMIT ? OFFSET ?"
	}

	query += ";"

	err := storage.db.SelectContext(ctx, &list, query, limit, offset)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// SelectStateCount - получает количество состояний
func (storage *Storage) SelectStateCount(ctx context.Context) (int64, error) {
	var count int64

	err := storage.db.GetContext(ctx, &count, `SELECT COUNT(*) FROM states;`)
	if err != nil {
		return 0, err
	}

	return count, nil
}
