package storage

import (
	"app/storage/migrator"
	"context"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

// Storage - хранилище данных
type Storage struct {
	db *sqlx.DB
}

// Connect - возвращает соединение с хранилищем данных
func Connect(ctx context.Context, dataSourceName string) (*Storage, error) {
	db, err := sqlx.Open("sqlite3", dataSourceName+"?_fk=1")
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(10)

	return &Storage{db: db}, nil
}

// MigrateAll - производит миграции данных
func (storage *Storage) MigrateAll(ctx context.Context) error {
	return migrator.MigrateAll(ctx, storage.db, true)
}

// tnow - текущее время в формате БД
func tnow() string { return time.Now().Format(time.RFC3339Nano) }
