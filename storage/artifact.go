package storage

import (
	"context"
	"database/sql"
)

// Artifact - данные хранилища артефактов
type Artifact struct {
	// ИД записи (индекс)
	ID int64 `db:"id"`
	// Значение записи
	Value string `db:"value"`
	// Время создания записи
	Created string `db:"created"`
	// Время последнего обновления записи
	Updated sql.NullString `db:"updated"`
}

// InsertArtifact - добавляет новый артефакт
func (storage *Storage) InsertArtifact(ctx context.Context, value string) (int64, error) {
	result, err := storage.db.ExecContext(ctx, `INSERT INTO artifacts(value, created) VALUES(?, ?);`, value, tnow())
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return id, nil
}

// UpdateArtifact - обновляет артефакт
func (storage *Storage) UpdateArtifact(ctx context.Context, id int64, value string) error {
	_, err := storage.db.ExecContext(ctx, `UPDATE artifacts SET value = ?, updated = ? WHERE id = ?;`, value, tnow(), id)
	if err != nil {
		return err
	}

	return nil
}

// DeleteArtifact - удаляет артефакт
func (storage *Storage) DeleteArtifact(ctx context.Context, id int64) error {
	_, err := storage.db.ExecContext(ctx, `DELETE FROM artifacts WHERE id = ?;`, id)
	if err != nil {
		return err
	}

	return nil
}

// SelectArtifact - получает артефакт по ИД
func (storage *Storage) SelectArtifact(ctx context.Context, id int64) (*Artifact, error) {
	item := new(Artifact)
	err := storage.db.GetContext(ctx, item, `SELECT * FROM artifacts WHERE id = ?;`, id)
	if err != nil {
		return nil, err
	}

	return item, nil
}

// SelectArtifacts - получает артефакты
func (storage *Storage) SelectArtifacts(ctx context.Context, limit, offset int64) ([]*Artifact, error) {
	list := make([]*Artifact, 0)

	query := `SELECT * FROM artifacts ORDER BY id`

	if limit > 0 {
		query += " LIMIT ? OFFSET ?"
	}

	query += ";"

	err := storage.db.SelectContext(ctx, &list, query, limit, offset)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// SelectArtifactCount - получает количество артефактов
func (storage *Storage) SelectArtifactCount(ctx context.Context) (int64, error) {
	var count int64

	err := storage.db.GetContext(ctx, &count, `SELECT COUNT(*) FROM artifacts;`)
	if err != nil {
		return 0, err
	}

	return count, nil
}
