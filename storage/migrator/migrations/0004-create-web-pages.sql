CREATE TABLE web_pages(
    path        TEXT        PRIMARY KEY,
    file_id     INTEGER     NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    name        TEXT,
    created     TEXT        NOT NULL,
    updated     TEXT
);