CREATE TABLE states(
    key         TEXT        PRIMARY KEY,
    value       TEXT        NOT NULL,
    created     TEXT        NOT NULL,
    updated     TEXT
);