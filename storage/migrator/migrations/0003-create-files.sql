CREATE TABLE files(
    id          INTEGER     PRIMARY KEY AUTOINCREMENT,
    body        BLOB        NOT NULL,
    filename    TEXT        NOT NULL,
    mime        TEXT        NOT NULL,
    size        INTEGER     NOT NULL,
    created     TEXT        NOT NULL,
    updated     TEXT
);