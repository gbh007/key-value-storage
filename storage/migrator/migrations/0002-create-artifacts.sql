CREATE TABLE artifacts(
    id          INTEGER     PRIMARY KEY AUTOINCREMENT,
    value       TEXT        NOT NULL,
    created     TEXT        NOT NULL,
    updated     TEXT
);