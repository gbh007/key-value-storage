package migrator

import (
	"app/logger"
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// MigrateAll - производит накат всех доступных миграций
func MigrateAll(ctx context.Context, db *sqlx.DB, checkHash bool) error {
	list, err := getFileList(ctx)
	if err != nil {
		return err
	}

	logger.Debug(ctx, "Доступные миграции")
	for _, item := range list {
		logger.Debug(ctx, fmt.Sprintf("%4d > %s", item.Number, item.Name))
	}

	var success bool

	tx, err := db.BeginTxx(ctx, nil)
	if err != nil {
		return err
	}

	// Функция для финилизации транзакции
	defer func() {
		if success {
			logger.IfErrFunc(ctx, tx.Commit)
		} else {
			logger.IfErrFunc(ctx, tx.Rollback)
		}
	}()

	_, err = tx.ExecContext(ctx, techMigration)
	if err != nil {
		return err
	}

	// Получаем список примененных миграций из БД
	aplliedMigrationsList, err := getAppliedMigration(ctx, tx)
	if err != nil {
		return err
	}

	aplliedMigrationsMap := make(map[int]mteMigration)
	for _, migration := range aplliedMigrationsList {
		aplliedMigrationsMap[migration.ID] = migration
	}

	for _, migrationFile := range list {
		mteInfo, migrationApplied := aplliedMigrationsMap[migrationFile.Number]

		var (
			hash, body string
		)

		// Не применена миграция или нужно сверить ее хеш
		if !migrationApplied || checkHash {
			body, hash, err = migrationFromFile(ctx, migrationFile)
			if err != nil {
				return err
			}
		}

		hashEqual := mteInfo.Hash == hash

		switch {
		// Миграция не применена
		case !migrationApplied:
			err = applyMigration(ctx, tx, migrationFile.Number, body, migrationFile.Name, hash)
			if err != nil {
				logger.ErrorText(ctx, fmt.Sprintf("%s - ERR", migrationFile.Name))

				return err
			}

			logger.Info(ctx, fmt.Sprintf("%s - OK", migrationFile.Name))

		// Миграция применена и хеш не эквивалентен
		case migrationApplied && checkHash && !hashEqual:
			logger.Debug(ctx, "old - hash >>>", mteInfo.Hash)
			logger.Debug(ctx, "new - hash >>>", hash)
			logger.Warning(ctx, fmt.Sprintf("%s - Not Equal HASH", migrationFile.Name))

		// Миграция уже применена
		case migrationApplied:
			logger.Debug(ctx, fmt.Sprintf("%s - EXIST %v", migrationFile.Name, mteInfo.Applied))

		}
	}

	success = true

	return nil
}
