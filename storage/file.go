package storage

import (
	"context"
	"database/sql"
)

// File - данные файла
type File struct {
	// ИД файла (индекс)
	ID int64 `db:"id"`
	// Значение файла
	Body []byte `db:"body"`
	// Название файла
	Filename string `db:"filename"`
	// MIME тип файла
	Mime string `db:"mime"`
	// Размер файла
	Size int64 `db:"size"`
	// Время создания файла
	Created string `db:"created"`
	// Время последнего обновления файла
	Updated sql.NullString `db:"updated"`
}

// InsertFile - добавляет новый файл
func (storage *Storage) InsertFile(ctx context.Context, body []byte, filename, mime string, size int64) (int64, error) {
	result, err := storage.db.ExecContext(ctx, `INSERT INTO files(body, filename, mime, size, created) VALUES(?, ?, ?, ?, ?);`, body, filename, mime, size, tnow())
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return id, nil
}

// UpdateFile - обновляет файл
func (storage *Storage) UpdateFile(ctx context.Context, id int64, body []byte, filename, mime string, size int64) error {
	_, err := storage.db.ExecContext(ctx, `UPDATE files SET body = ?, filename = ?, mime = ?, size = ?, updated = ? WHERE id = ?;`, body, filename, mime, size, tnow(), id)
	if err != nil {
		return err
	}

	return nil
}

// DeleteFile - удаляет файл
func (storage *Storage) DeleteFile(ctx context.Context, id int64) error {
	_, err := storage.db.ExecContext(ctx, `DELETE FROM files WHERE id = ?;`, id)
	if err != nil {
		return err
	}

	return nil
}

// SelectFile - получает файл по ИД
func (storage *Storage) SelectFile(ctx context.Context, id int64) (*File, error) {
	item := new(File)
	err := storage.db.GetContext(ctx, item, `SELECT * FROM files WHERE id = ?;`, id)
	if err != nil {
		return nil, err
	}

	return item, nil
}

// SelectFiles - получает файлы, не включает тело
func (storage *Storage) SelectFiles(ctx context.Context, limit, offset int64) ([]*File, error) {
	list := make([]*File, 0)

	query := `SELECT id, filename, mime, size, created, updated FROM files ORDER BY id`

	if limit > 0 {
		query += " LIMIT ? OFFSET ?"
	}

	query += ";"

	err := storage.db.SelectContext(ctx, &list, query, limit, offset)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// SelectFileCount - получает количество файлов
func (storage *Storage) SelectFileCount(ctx context.Context) (int64, error) {
	var count int64

	err := storage.db.GetContext(ctx, &count, `SELECT COUNT(*) FROM files;`)
	if err != nil {
		return 0, err
	}

	return count, nil
}
