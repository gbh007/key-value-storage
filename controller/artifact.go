package controller

import (
	"app/storage"
	"context"
	"time"
)

// Artifact - данные артефакта
type Artifact struct {
	// ИД записи (индекс)
	ID int64
	// Значение записи
	Value string
	// Время создания записи
	Created time.Time
	// Время последнего обновления записи
	Updated *time.Time
}

func artifactFromStorage(raw *storage.Artifact) *Artifact {
	item := Artifact{
		ID:    raw.ID,
		Value: raw.Value,
	}

	item.Created, _ = time.Parse(time.RFC3339Nano, raw.Created)

	if raw.Updated.Valid {
		t, _ := time.Parse(time.RFC3339Nano, raw.Updated.String)
		item.Updated = &t
	}

	return &item
}

// CreateArtifact - создает артефакт
func (controller *Controller) CreateArtifact(ctx context.Context, value string) (int64, error) {
	id, err := controller.storage.InsertArtifact(ctx, value)
	if err != nil {
		return 0, err
	}

	return id, nil
}

// UpdateArtifact - обновляет артефакт
func (controller *Controller) UpdateArtifact(ctx context.Context, id int64, value string) error {
	err := controller.storage.UpdateArtifact(ctx, id, value)
	if err != nil {
		return err
	}

	return nil
}

// GetArtifact - возвращает артефакт
func (controller *Controller) GetArtifact(ctx context.Context, id int64) (*Artifact, error) {
	rawValue, err := controller.storage.SelectArtifact(ctx, id)
	if err != nil {
		return nil, err
	}

	return artifactFromStorage(rawValue), nil
}

// DeleteArtifact - удаляет артефакт
func (controller *Controller) DeleteArtifact(ctx context.Context, id int64) error {
	err := controller.storage.DeleteArtifact(ctx, id)
	if err != nil {
		return err
	}

	return nil
}

// GetArtifactsPage - возвращает страницу с артефактами
func (controller *Controller) GetArtifactsPage(ctx context.Context, page int64, onPageCount int64) ([]*Artifact, *PagesInfo, error) {
	itemCount, err := controller.storage.SelectArtifactCount(ctx)
	if err != nil {
		return nil, nil, err
	}

	// Нет данных, используем дефолтное значение
	if onPageCount == 0 {
		onPageCount = DefaultOnPageCount
	}

	limit, offset := pageToLimit(page, onPageCount)

	// Установлено без ограничений
	if onPageCount == -1 {
		limit = -1
		offset = 0
	}

	rawValues, err := controller.storage.SelectArtifacts(ctx, limit, offset)
	if err != nil {
		return nil, nil, err
	}

	list := make([]*Artifact, 0, len(rawValues))

	for _, raw := range rawValues {
		list = append(list, artifactFromStorage(raw))
	}

	pageInfo := &PagesInfo{
		TotalItems:  itemCount,
		CurrentPage: page,
		TotalPages:  totalToPages(itemCount, onPageCount),
	}

	return list, pageInfo, nil
}
