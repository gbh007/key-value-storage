package controller

import (
	"app/storage"
	"context"
	"database/sql"
	"errors"
	"time"
)

// State - данные состояния
type State struct {
	// Ключ записи
	Key string
	// Значение записи
	Value string
	// Время создания записи
	Created time.Time
	// Время последнего обновления записи
	Updated *time.Time
}

func stateFromStorage(raw *storage.State) *State {
	item := State{
		Key:   raw.Key,
		Value: raw.Value,
	}

	item.Created, _ = time.Parse(time.RFC3339Nano, raw.Created)

	if raw.Updated.Valid {
		t, _ := time.Parse(time.RFC3339Nano, raw.Updated.String)
		item.Updated = &t
	}

	return &item
}

// SetState - устанавливает состояние
func (controller *Controller) SetState(ctx context.Context, oldKey, newKey, value string) error {
	_, err := controller.storage.SelectState(ctx, oldKey)

	if errors.Is(err, sql.ErrNoRows) {
		insertErr := controller.storage.InsertState(ctx, newKey, value)
		if insertErr != nil {
			return insertErr
		}

		return nil
	}

	if err != nil {
		return err
	}

	err = controller.storage.UpdateState(ctx, oldKey, newKey, value)
	if err != nil {
		return err
	}

	return nil
}

// GetState - возвращает состояние
func (controller *Controller) GetState(ctx context.Context, key string) (*State, error) {
	rawValue, err := controller.storage.SelectState(ctx, key)
	if err != nil {
		return nil, err
	}

	return stateFromStorage(rawValue), nil
}

// DeleteState - удаляет состояние
func (controller *Controller) DeleteState(ctx context.Context, key string) error {
	err := controller.storage.DeleteState(ctx, key)
	if err != nil {
		return err
	}

	return nil
}

// GetStatesPage - возвращает страницу с состояниями
func (controller *Controller) GetStatesPage(ctx context.Context, page int64, onPageCount int64) ([]*State, *PagesInfo, error) {
	itemCount, err := controller.storage.SelectStateCount(ctx)
	if err != nil {
		return nil, nil, err
	}

	// Нет данных, используем дефолтное значение
	if onPageCount == 0 {
		onPageCount = DefaultOnPageCount
	}

	limit, offset := pageToLimit(page, onPageCount)

	// Установлено без ограничений
	if onPageCount == -1 {
		limit = -1
		offset = 0
	}

	rawValues, err := controller.storage.SelectStates(ctx, limit, offset)
	if err != nil {
		return nil, nil, err
	}

	list := make([]*State, 0, len(rawValues))

	for _, raw := range rawValues {
		list = append(list, stateFromStorage(raw))
	}

	pageInfo := &PagesInfo{
		TotalItems:  itemCount,
		CurrentPage: page,
		TotalPages:  totalToPages(itemCount, onPageCount),
	}

	return list, pageInfo, nil
}
