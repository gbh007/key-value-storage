package controller

import (
	"app/logger"
	"app/storage"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

// MakeRequest - выполняет прокси запрос
func (controller *Controller) MakeRequest(ctx context.Context, toRequest ToRequest) (*ToResponse, error) {
	// Асинхронный запрос без сохранения не возможен (не имеет смысла для обычного использования, только для ДДОС)
	if toRequest.Async && (toRequest.NoStore || toRequest.NoStoreArtifact) {
		return nil, fmt.Errorf("can't async with no store")
	}

	var (
		requestFileID int64
		artifactID    int64
		err           error
	)

	// Если требуется тело из запроса то загружаем его
	if toRequest.RequestBodyFromFile != nil {
		bodyFile, err := controller.GetFile(ctx, *toRequest.RequestBodyFromFile)
		if err != nil {
			return nil, err
		}

		toRequest.Data.Body = bodyFile.Body
		requestFileID = *toRequest.RequestBodyFromFile
	} else if !toRequest.NoStore && !toRequest.NoStoreRequest && len(toRequest.Data.Body) > 0 {
		// Если не указано не сохранение файла запроса, то сохраняем его, в случае если он не пустой
		requestFileID, err = controller.CreateFile(ctx, File{
			Body:     toRequest.Data.Body,
			Filename: "request",
			Mime:     toRequest.Data.ContextType(),
		})
		if err != nil {
			return nil, err
		}
	}

	// Если можно сохранять данные и артефакт
	if !toRequest.NoStore && !toRequest.NoStoreArtifact {
		atr := storage.RequesterArtifact{
			Done:        false,
			RequestData: toRequest.Data.ToDB(requestFileID),
		}

		artifactData, err := json.MarshalIndent(atr, "", "  ")
		if err != nil {
			return nil, err
		}

		artifactID, err = controller.CreateArtifact(ctx, string(artifactData))
		if err != nil {
			return nil, err
		}
	}

	// Если запрос асинхронный
	if toRequest.Async {
		go func() {
			bgContext := context.TODO()

			_, _, _, err := controller.syncRequest(bgContext, toRequest, requestFileID, artifactID)
			logger.IfErr(bgContext, err)
		}()

		return &ToResponse{
			ArtifactID: artifactID,
		}, nil
	}

	responseData, responseFileID, responseCode, err := controller.syncRequest(ctx, toRequest, requestFileID, artifactID)
	if err != nil {
		return nil, err
	}

	return &ToResponse{
		Data:               *responseData,
		ResponseBodyFileID: responseFileID,
		ArtifactID:         artifactID,
		ResponseCode:       responseCode,
	}, nil
}

// syncRequest - выполняет синхронный запрос с сохранением данных в БД (если не запрещено)
func (controller *Controller) syncRequest(ctx context.Context, toRequest ToRequest, requestFileID, artifactID int64) (*HTTPData, int64, int, error) {
	var (
		responseFileID int64
		err            error
	)

	responseData, responseCode, err := makeRequest(ctx, toRequest.Data)
	if err != nil {
		// В асинхронном режиме сохраняем данные в случае ошибки
		if toRequest.Async {
			controller.saveResponseErr(ctx, toRequest, requestFileID, artifactID, responseCode, err)
		}

		return nil, 0, 0, err
	}

	// Если нельзя сохранять данные
	if toRequest.NoStore {
		return responseData, responseFileID, responseCode, nil
	}

	responseFile := File{
		Body:     responseData.Body,
		Filename: "response",
		Mime:     responseData.ContextType(),
	}

	switch {
	case len(responseData.Body) == 0:
		// Не делаем ничего, тело пустое

	case toRequest.ResponseBodyToFile != nil:
		responseFileID = *toRequest.ResponseBodyToFile
		responseFile.ID = responseFileID
		err = controller.UpdateFile(ctx, responseFile)

	default:
		responseFileID, err = controller.CreateFile(ctx, responseFile)
	}

	if err != nil {
		// В асинхронном режиме сохраняем данные в случае ошибки
		if toRequest.Async {
			controller.saveResponseErr(ctx, toRequest, requestFileID, artifactID, responseCode, err)
		}

		return nil, 0, 0, err
	}

	// Если нельзя сохранять артефакт
	if toRequest.NoStoreArtifact {
		return responseData, responseFileID, responseCode, nil
	}

	atr := storage.RequesterArtifact{
		Done:         true,
		RequestData:  toRequest.Data.ToDB(requestFileID),
		ResponseData: responseData.ToDB(responseFileID),
		ResponseCode: responseCode,
	}

	artifactData, err := json.MarshalIndent(atr, "", "  ")
	if err != nil {
		return nil, 0, 0, err
	}

	err = controller.UpdateArtifact(ctx, artifactID, string(artifactData))
	if err != nil {
		return nil, 0, 0, err
	}

	return responseData, responseFileID, responseCode, nil
}

// saveResponseErr - сохраняет в БД результат неуспешного прокси-запроса
func (controller *Controller) saveResponseErr(ctx context.Context, toRequest ToRequest, requestFileID, artifactID int64, responseCode int, originErr error) {
	// Если нельзя сохранять данные или артефакт
	if toRequest.NoStore || toRequest.NoStoreArtifact {
		return
	}

	atr := storage.RequesterArtifact{
		Done:         true,
		RequestData:  toRequest.Data.ToDB(requestFileID),
		ResponseCode: responseCode,
		Error:        originErr.Error(),
	}

	artifactData, err := json.MarshalIndent(atr, "", "  ")
	if err != nil {
		logger.Error(ctx, err)

		return
	}

	err = controller.UpdateArtifact(ctx, artifactID, string(artifactData))
	if err != nil {
		logger.Error(ctx, err)

		return
	}
}

// makeRequest - выполняет запрос
func makeRequest(ctx context.Context, toRequest HTTPData) (*HTTPData, int, error) {
	req, err := http.NewRequest(toRequest.Method, toRequest.URL, bytes.NewReader(toRequest.Body))
	if err != nil {
		return nil, 0, err
	}

	// Добавляем заголовки в данные запроса
	for _, h := range toRequest.Headers {
		for _, v := range h.Values {
			req.Header.Add(h.Name, v)
		}
	}

	// Добавляем печеньки в данные запроса
	for _, c := range toRequest.Cookies {
		req.AddCookie(&http.Cookie{
			Name:  c.Name,
			Value: c.Value,
		})
	}

	req.Close = true

	// выполняем запрос
	response, err := (&http.Client{
		Timeout: time.Minute * 5,
	}).Do(req)
	if err != nil {
		return nil, 0, err
	}

	defer response.Body.Close()

	data, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, 0, err
	}

	responseData := &HTTPData{
		Body: data,
	}

	// Получаем заголовки из данных ответа
	for hName, hValues := range response.Header {
		responseData.Headers = append(responseData.Headers, Header{
			Name:   hName,
			Values: hValues,
		})
	}

	// Получаем печеньки из данных ответа
	for _, c := range response.Cookies() {
		responseData.Cookies = append(responseData.Cookies, Cookie{
			Name:  c.Name,
			Value: c.Value,
		})
	}

	return responseData, response.StatusCode, nil
}
