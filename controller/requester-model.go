package controller

import (
	"app/storage"
	"strings"
)

// ToRequest - запрос на выполнение прокси-запроса
type ToRequest struct {
	// Данные запроса
	Data HTTPData
	// ИД файла из которого необходимо сделать запрос (если есть)
	RequestBodyFromFile *int64
	// ИД файла в который необходимо сохранить ответ (если есть),
	// если указан, то NoStore будет проигнорирован для сохранения тела ответа
	ResponseBodyToFile *int64
	// Не сохранять данные (файлы и прочее)
	NoStore bool
	// Не сохранять файл запроса
	NoStoreRequest bool
	// Не сохранять данные артефакта
	NoStoreArtifact bool
	// Выполнить запрос асинхронно
	Async bool
}

// ToResponse - ответ на выполнение прокси-запроса
type ToResponse struct {
	// Данные ответа (если есть)
	Data HTTPData
	// ИД файла в который сохранен ответ (если есть),
	ResponseBodyFileID int64
	// HTTP код ответа
	ResponseCode int
	// ИД артефакта с информацией о запросе/ответе
	ArtifactID int64
}

// HTTPData - данные для запроса/ответа
type HTTPData struct {
	// Адрес на который необходимо делать запрос
	URL string
	// Метод запроса
	Method string
	// Заголовки запроса/ответа
	Headers []Header
	// Печеньки запроса/ответа
	Cookies []Cookie
	// Тело запроса/ответа
	Body []byte
}

func (data HTTPData) ToDB(fileID int64) storage.HTTPData {
	headers := make([]storage.Header, 0, len(data.Headers))
	for _, h := range data.Headers {
		headers = append(headers, h.ToDB())
	}

	cookies := make([]storage.Cookie, 0, len(data.Cookies))
	for _, h := range data.Cookies {
		cookies = append(cookies, h.ToDB())
	}

	return storage.HTTPData{
		URL:        data.URL,
		Method:     data.Method,
		Headers:    headers,
		Cookies:    cookies,
		BodyFileID: fileID,
	}
}

func (data HTTPData) ContextType() string {
	for _, h := range data.Headers {
		if strings.ToLower(h.Name) == "content-type" && len(h.Values) > 0 {
			return h.Values[0]
		}
	}

	return ""
}

// Header - данные заголовков
type Header struct {
	// Название заголовка
	Name string
	// Значения заголовков
	Values []string
}

func (h Header) ToDB() storage.Header {
	values := make([]string, len(h.Values))
	copy(values, h.Values)

	return storage.Header{
		Name:   h.Name,
		Values: values,
	}
}

// Cookie - данные печенек
type Cookie struct {
	// Название печеньки
	Name string
	// Значение печеньки
	Value string
}

func (c Cookie) ToDB() storage.Cookie {
	return storage.Cookie{
		Name:  c.Name,
		Value: c.Value,
	}
}
