package controller

import (
	"app/storage"
)

// Controller - контроллер логики
type Controller struct {
	// Хранилище данных
	storage *storage.Storage
}

// Init - инициализирует контроллер
func Init(storage *storage.Storage) *Controller {
	return &Controller{storage: storage}
}
