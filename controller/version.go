package controller

var (
	// Версия приложения
	Version string
	// Номер коммита
	Commit string
	// Время сборки
	BuildAt string
)
