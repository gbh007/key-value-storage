package controller

// PagesInfo - информация о страницах
type PagesInfo struct {
	// Текущая страница
	CurrentPage int64
	// Всего страниц
	TotalPages int64
	// Всего значений
	TotalItems int64
}

// Стандартное количество сообщений на странице
var DefaultOnPageCount int64 = 20

// pageToLimit - конвертирует старицу в сдвиг для БД
func pageToLimit(page int64, onPageCount int64) (limit, offset int64) {
	limit = onPageCount
	offset = (page - 1) * onPageCount

	return
}

// totalToPages - конвертирует количество данных в количество страниц
func totalToPages(total int64, onPageCount int64) (pageCount int64) {
	if onPageCount < 1 {
		return 1
	}

	pageCount = total / onPageCount

	if total%onPageCount > 0 {
		pageCount++
	}

	return
}
