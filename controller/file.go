package controller

import (
	"app/storage"
	"context"
	"time"
)

// File - данные файла
type File struct {
	// ИД файла
	ID int64
	// Тело файла
	Body []byte
	// Название файла
	Filename string
	// MIME тип файла
	Mime string
	// Размер файла
	Size int64
	// Время создания файла
	Created time.Time
	// Время последнего обновления файла
	Updated *time.Time
}

func fileFromStorage(raw *storage.File) *File {
	item := File{
		ID:       raw.ID,
		Body:     raw.Body,
		Filename: raw.Filename,
		Mime:     raw.Mime,
		Size:     raw.Size,
	}

	item.Created, _ = time.Parse(time.RFC3339Nano, raw.Created)

	if raw.Updated.Valid {
		t, _ := time.Parse(time.RFC3339Nano, raw.Updated.String)
		item.Updated = &t
	}

	return &item
}

// CreateFile - создает файл
func (controller *Controller) CreateFile(ctx context.Context, file File) (int64, error) {
	if len(file.Body) == 0 {
		file.Body = []byte{}
	}

	id, err := controller.storage.InsertFile(ctx, file.Body, file.Filename, file.Mime, int64(len(file.Body)))
	if err != nil {
		return 0, err
	}

	return id, nil
}

// UpdateFile - обновляет файл
func (controller *Controller) UpdateFile(ctx context.Context, file File) error {
	if len(file.Body) == 0 {
		file.Body = []byte{}
	}

	err := controller.storage.UpdateFile(ctx, file.ID, file.Body, file.Filename, file.Mime, int64(len(file.Body)))
	if err != nil {
		return err
	}

	return nil
}

// GetFile - возвращает файл
func (controller *Controller) GetFile(ctx context.Context, id int64) (*File, error) {
	rawValue, err := controller.storage.SelectFile(ctx, id)
	if err != nil {
		return nil, err
	}

	return fileFromStorage(rawValue), nil
}

// DeleteFile - удаляет файл
func (controller *Controller) DeleteFile(ctx context.Context, id int64) error {
	err := controller.storage.DeleteFile(ctx, id)
	if err != nil {
		return err
	}

	return nil
}

// GetFilesPage - возвращает страницу с файлами
func (controller *Controller) GetFilesPage(ctx context.Context, page int64, onPageCount int64) ([]*File, *PagesInfo, error) {
	itemCount, err := controller.storage.SelectFileCount(ctx)
	if err != nil {
		return nil, nil, err
	}

	// Нет данных, используем дефолтное значение
	if onPageCount == 0 {
		onPageCount = DefaultOnPageCount
	}

	limit, offset := pageToLimit(page, onPageCount)

	// Установлено без ограничений
	if onPageCount == -1 {
		limit = -1
		offset = 0
	}

	rawValues, err := controller.storage.SelectFiles(ctx, limit, offset)
	if err != nil {
		return nil, nil, err
	}

	list := make([]*File, 0, len(rawValues))

	for _, raw := range rawValues {
		list = append(list, fileFromStorage(raw))
	}

	pageInfo := &PagesInfo{
		TotalItems:  itemCount,
		CurrentPage: page,
		TotalPages:  totalToPages(itemCount, onPageCount),
	}

	return list, pageInfo, nil
}
