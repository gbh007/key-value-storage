package controller

import (
	"app/storage"
	"context"
	"database/sql"
	"errors"
	"strings"
	"time"
)

// WebPage - данные веб страницы
type WebPage struct {
	// Путь до страницы
	Path string
	// ИД файла со страницей
	FileID int64
	// Название страницы
	Name string
	// Время создания страницы
	Created time.Time
	// Время последнего обновления страницы
	Updated *time.Time
}

func webPageFromStorage(raw *storage.WebPage) *WebPage {
	item := WebPage{
		Path:   strings.ToLower(raw.Path),
		FileID: raw.FileID,
		Name:   raw.Name.String,
	}

	item.Created, _ = time.Parse(time.RFC3339Nano, raw.Created)

	if raw.Updated.Valid {
		t, _ := time.Parse(time.RFC3339Nano, raw.Updated.String)
		item.Updated = &t
	}

	return &item
}

// SetWebPage - устанавливает веб страницу
func (controller *Controller) SetWebPage(ctx context.Context, oldPath, newPath string, fileID int64, name string) error {
	oldPath = strings.ToLower(oldPath)
	newPath = strings.ToLower(newPath)

	sqlName := sql.NullString{
		String: name,
		Valid:  name != "",
	}

	_, err := controller.storage.SelectWebPage(ctx, oldPath)

	if errors.Is(err, sql.ErrNoRows) {
		insertErr := controller.storage.InsertWebPage(ctx, newPath, fileID, sqlName)
		if insertErr != nil {
			return insertErr
		}

		return nil
	}

	if err != nil {
		return err
	}

	err = controller.storage.UpdateWebPage(ctx, oldPath, newPath, fileID, sqlName)
	if err != nil {
		return err
	}

	return nil
}

// GetWebPage - возвращает веб страницу
func (controller *Controller) GetWebPage(ctx context.Context, path string) (*WebPage, error) {
	path = strings.ToLower(path)

	rawFileID, err := controller.storage.SelectWebPage(ctx, path)
	if err != nil {
		return nil, err
	}

	return webPageFromStorage(rawFileID), nil
}

// DeleteWebPage - удаляет веб страницу
func (controller *Controller) DeleteWebPage(ctx context.Context, path string) error {
	path = strings.ToLower(path)

	err := controller.storage.DeleteWebPage(ctx, path)
	if err != nil {
		return err
	}

	return nil
}

// GetWebPagesPage - возвращает страницу с состояниями
func (controller *Controller) GetWebPagesPage(ctx context.Context, page int64, onPageCount int64, onlyNamed bool) ([]*WebPage, *PagesInfo, error) {
	itemCount, err := controller.storage.SelectWebPageCount(ctx, onlyNamed)
	if err != nil {
		return nil, nil, err
	}

	// Нет данных, используем дефолтное значение
	if onPageCount == 0 {
		onPageCount = DefaultOnPageCount
	}

	limit, offset := pageToLimit(page, onPageCount)

	// Установлено без ограничений
	if onPageCount == -1 {
		limit = -1
		offset = 0
	}

	rawList, err := controller.storage.SelectWebPages(ctx, limit, offset, onlyNamed)
	if err != nil {
		return nil, nil, err
	}

	list := make([]*WebPage, 0, len(rawList))

	for _, raw := range rawList {
		list = append(list, webPageFromStorage(raw))
	}

	pageInfo := &PagesInfo{
		TotalItems:  itemCount,
		CurrentPage: page,
		TotalPages:  totalToPages(itemCount, onPageCount),
	}

	return list, pageInfo, nil
}
