package logger

import (
	"context"
	"fmt"
	"os"
	"runtime"
	"time"
)

// timeFormat - формат времени для лога
const timeFormat = "2006-01-02 15:04:05"

// приписки для уровней логирования
const (
	logLevelDebug   = "DBG"
	logLevelInfo    = "INF"
	logLevelWarning = "WRN"
	logLevelError   = "ERR"
)

// IfErr - выводит в лог ошибку, но только если она есть
func IfErr(ctx context.Context, err error) {
	if err == nil {
		return
	}

	print(ctx, logLevelError, 0, err.Error())
}

// IfErrFunc - враппер ошибки в лог для методов подобных Close
func IfErrFunc(ctx context.Context, f func() error) {
	err := f()
	if err == nil {
		return
	}

	print(ctx, logLevelError, 0, err.Error())
}

// Error - вывести в лог ошибку
func Error(ctx context.Context, err error) {
	print(ctx, logLevelError, 0, err.Error())
}

// ErrorText - вывести в лог текст как ошибку
func ErrorText(ctx context.Context, args ...interface{}) {
	print(ctx, logLevelError, 0, args...)
}

// Info - вывести в лог вспомогательную информацию
func Info(ctx context.Context, args ...interface{}) {
	print(ctx, logLevelInfo, 0, args...)
}

// Warning - вывести в лог предупреждение
func Warning(ctx context.Context, args ...interface{}) {
	print(ctx, logLevelWarning, 0, args...)
}

// print - выводит данные в стандартный поток ошибок
func print(ctx context.Context, level string, depth int, args ...interface{}) {
	fmt.Fprintf(
		os.Stderr,
		"[%s][%s] %s - %s",
		level,
		from(ctx, 3+depth),
		time.Now().Format(timeFormat),
		fmt.Sprintln(args...),
	)
}

// from - вычисляет место вызова, выводит данные только в режиме отладки
func from(ctx context.Context, depth int) string {
	if !DebugMode {
		return "-"
	}

	_, file, line, ok := runtime.Caller(depth)
	if ok {
		return fmt.Sprintf("%s:%d", file, line)
	}

	return "???"
}
