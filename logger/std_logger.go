package logger

import (
	"context"
	"log"
)

// lWriter - реализация io.Writer для логирования веба
type lWriter struct {
	ctx   context.Context
	level string
}

// Write - реализация io.Writer
func (lw *lWriter) Write(data []byte) (int, error) {
	print(lw.ctx, lw.level, 2, string(data))

	return len(data), nil
}

// StdErrorLogger - реализация стандартного логгера, через пакет логирования
func StdErrorLogger(ctx context.Context) *log.Logger {
	return log.New(
		&lWriter{
			ctx:   ctx,
			level: logLevelError,
		},
		"",
		0,
	)
}
