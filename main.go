package main

import (
	"app/controller"
	"app/logger"
	"app/static"
	"app/storage"
	"app/webserver"
	"context"
	"errors"
	"flag"
	"os"
	"os/signal"
	"path"
	"syscall"
)

func main() {
	webAddr := flag.String("addr", ":8080", "Адрес веб сервера")
	dbFileName := flag.String("db", "main.db", "Файл базы")
	debugMode := flag.Bool("d", false, "Активировать режим отладки (дебага)")
	flag.Parse()

	logger.DebugMode = *debugMode

	ctx, cancelNotify := signal.NotifyContext(
		context.Background(),
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)
	defer cancelNotify()

	logger.Info(ctx, "Версия", controller.Version)
	logger.Info(ctx, "Коммит", controller.Commit)
	logger.Info(ctx, "Собрано", controller.BuildAt)

	stor, err := storage.Connect(ctx, *dbFileName)
	if err != nil {
		logger.Error(ctx, err)

		os.Exit(1)
	}

	err = stor.MigrateAll(ctx)
	if err != nil {
		logger.Error(ctx, err)

		os.Exit(1)
	}

	err = createStatic(ctx)
	if err != nil {
		logger.Error(ctx, err)

		os.Exit(1)
	}

	ctl := controller.Init(stor)

	ws := webserver.Init(ctl)

	<-ws.StartServer(ctx, *webAddr)
}

func createStatic(ctx context.Context) error {
	const staticPath = "static"

	_, err := os.Stat(staticPath)

	if errors.Is(err, os.ErrNotExist) {
		err = os.MkdirAll(staticPath, os.ModeDir|os.ModePerm)
		if err != nil {
			return err
		}

		err = dirHandle(ctx, ".", staticPath)
		if err != nil {
			return err
		}

		return nil
	}

	if err != nil {
		return err
	}

	logger.Debug(ctx, staticPath, "уже существует")

	return nil
}

func dirHandle(ctx context.Context, innerPath string, outerPath string) error {
	files, err := static.StaticDir.ReadDir(innerPath)
	if err != nil {
		return err
	}

	for _, f := range files {
		fileInnerPath := path.Join(innerPath, f.Name())
		fileOuterPath := path.Join(outerPath, f.Name())

		if f.IsDir() {
			err = os.MkdirAll(fileOuterPath, os.ModeDir|os.ModePerm)
			if err != nil {
				return err
			}

			logger.Info(ctx, "Папка создана", fileOuterPath)

			err = dirHandle(ctx, fileInnerPath, fileOuterPath)
			if err != nil {
				return err
			}
		} else {
			data, err := static.StaticDir.ReadFile(fileInnerPath)
			if err != nil {
				return err
			}

			outerFile, err := os.Create(fileOuterPath)
			if err != nil {
				return err
			}

			defer outerFile.Close()

			_, err = outerFile.Write(data)
			if err != nil {
				return err
			}

			logger.Info(ctx, "Файл создан", fileOuterPath)
		}
	}

	return nil
}
