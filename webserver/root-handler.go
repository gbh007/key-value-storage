package webserver

import (
	"database/sql"
	"errors"
	"net/http"
)

func (ws *WebServer) rootHandler() http.Handler {
	defaultHandler := http.FileServer(http.Dir("./static"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		path := r.URL.Path
		// Если путь пустой то указываем что это корень
		if path == "" {
			path = "/"
		}

		pageInfo, err := ws.controller.GetWebPage(ctx, path)

		if errors.Is(err, sql.ErrNoRows) {
			defaultHandler.ServeHTTP(w, r)

			return
		}

		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		item, err := ws.controller.GetFile(ctx, pageInfo.FileID)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		w.Header().Set("Content-Type", item.Mime)
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(item.Body)
	})
}
