package webserver

import (
	"net/http"
)

type stateDeleteRequest struct {
	// Ключ записи
	Key string `json:"key"`
}

func (ws *WebServer) stateDelete() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(stateDeleteRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = ws.controller.DeleteState(ctx, req.Key)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		writeNoContent(w)
	})
}
