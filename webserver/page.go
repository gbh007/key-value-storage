package webserver

import "app/controller"

// pagesInfo - информация о страницах
type pagesInfo struct {
	// Текущая страница
	CurrentPage int64 `json:"current"`
	// Всего страниц
	TotalPages int64 `json:"pages"`
	// Всего значений
	TotalItems  int64   `json:"items"`
	PrettyPages []int64 `json:"pretty_pages"`
}

func pagesInfoFromController(raw *controller.PagesInfo) pagesInfo {
	return pagesInfo{
		CurrentPage: raw.CurrentPage,
		TotalPages:  raw.TotalPages,
		TotalItems:  raw.TotalItems,
		PrettyPages: generatePagination(raw.CurrentPage, raw.TotalPages),
	}
}
