package webserver

import (
	"app/controller"
	"net/http"
)

func (ws *WebServer) requester() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(requesterInput)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		toRequest := controller.ToRequest{
			Data: controller.HTTPData{
				URL:    req.URL,
				Method: req.Method,
			},
			ResponseBodyToFile: req.ResponseBodyToFile,
			NoStore:            req.NoStore,
			NoStoreRequest:     req.NoStoreRequest,
			NoStoreArtifact:    req.NoStoreArtifact,
			Async:              req.Async,
		}

		// В зависимости от конфигурации используем нужное тело запроса
		switch {
		case req.RequestBodyFromFile != nil:
			toRequest.RequestBodyFromFile = req.RequestBodyFromFile
		case req.RequestBodyRaw:
			toRequest.Data.Body = []byte(req.Data.Body)
		case len(req.Data.Body) > 0:
			body, err := fromBase64ToRaw(req.Data.Body)
			if err != nil {
				writeJSON(ctx, w, http.StatusBadRequest, err)

				return
			}

			toRequest.Data.Body = body
		}

		for _, h := range req.Data.Headers {
			toRequest.Data.Headers = append(toRequest.Data.Headers, controller.Header{
				Name:   h.Name,
				Values: h.Values,
			})
		}

		for _, c := range req.Data.Cookies {
			toRequest.Data.Cookies = append(toRequest.Data.Cookies, controller.Cookie{
				Name:  c.Name,
				Value: c.Value,
			})
		}

		toResponse, err := ws.controller.MakeRequest(ctx, toRequest)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		resp := &requesterOutput{
			Data:               HTTPData{},
			ResponseBodyFileID: toResponse.ResponseBodyFileID,
			ResponseCode:       toResponse.ResponseCode,
			ArtifactID:         toResponse.ArtifactID,
		}

		for _, h := range toResponse.Data.Headers {
			resp.Data.Headers = append(resp.Data.Headers, Header{
				Name:   h.Name,
				Values: h.Values,
			})
		}

		for _, c := range toResponse.Data.Cookies {
			resp.Data.Cookies = append(resp.Data.Cookies, Cookie{
				Name:  c.Name,
				Value: c.Value,
			})
		}

		switch {
		case req.NoResponseBody:
			// Тело ответа не нужно

		case req.ResponseBodyRaw:
			resp.Data.Body = string(toResponse.Data.Body)

		default:
			resp.Data.Body = fromRawToBase64(toResponse.Data.Body)
		}

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
