package webserver

import (
	"app/controller"
	"net/http"
	"time"
)

// artifactValue - данные индексированного значения
type artifactValue struct {
	// ИД записи (индекс)
	ID int64 `json:"id"`
	// Значение записи
	Value string `json:"value"`
	// Время создания записи
	Created time.Time `json:"created"`
	// Время последнего обновления записи
	Updated *time.Time `json:"updated,omitempty"`
}

func artifactFromController(raw *controller.Artifact) artifactValue {
	return artifactValue{
		ID:      raw.ID,
		Value:   raw.Value,
		Created: raw.Created,
		Updated: raw.Updated,
	}
}

type artifactListResponse struct {
	// Список артефактов
	Items []artifactValue `json:"items"`
	// Данные о страницах
	Page pagesInfo `json:"page"`
}

type artifactListRequest struct {
	// Запрашиваемая страница
	Page int64 `json:"page"`
	// Количество элементов на странице
	OnPageCount int64 `json:"on_page_count"`
}

func (ws *WebServer) artifactList() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(artifactListRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		items, info, err := ws.controller.GetArtifactsPage(ctx, req.Page, req.OnPageCount)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		resp := &artifactListResponse{
			Page: pagesInfoFromController(info),
		}

		for _, item := range items {
			resp.Items = append(resp.Items, artifactFromController(item))
		}

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
