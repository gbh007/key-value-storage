package webserver

// requesterInput - запрос на выполнение прокси-запроса
type requesterInput struct {
	// Адрес на который необходимо делать запрос
	URL string `json:"url"`
	// Метод запроса
	Method string `json:"method"`
	// Данные запроса
	Data HTTPData `json:"data,omitempty"`
	// ИД файла из которого необходимо сделать запрос (если есть)
	RequestBodyFromFile *int64 `json:"request_body_file_id,omitempty"`
	// ИД файла в который необходимо сохранить ответ (если есть),
	// если указан, то NoStore будет проигнорирован для сохранения тела ответа
	ResponseBodyToFile *int64 `json:"response_body_file_id,omitempty"`
	// Не сохранять данные (файлы и прочее)
	NoStore bool `json:"no_store,omitempty"`
	// Не сохранять данные артефакта
	NoStoreArtifact bool `json:"no_store_artifact,omitempty"`
	// Не сохранять файл запроса
	NoStoreRequest bool `json:"no_store_request,omitempty"`
	// Выполнить запрос асинхронно
	Async bool `json:"async,omitempty"`
	// Тело запроса сырая строка
	RequestBodyRaw bool `json:"request_body_raw,omitempty"`
	// Тело ответа сырая строка
	ResponseBodyRaw bool `json:"response_body_raw,omitempty"`
	// Не отдавать тело ответа
	NoResponseBody bool `json:"no_response_body,omitempty"`
}

// requesterOutput - ответ на выполнение прокси-запроса
type requesterOutput struct {
	// Данные ответа (если есть)
	Data HTTPData `json:"data,omitempty"`
	// ИД файла в который сохранен ответ (если есть),
	ResponseBodyFileID int64 `json:"response_body_file_id,omitempty"`
	// HTTP код ответа
	ResponseCode int `json:"response_code,omitempty"`
	// ИД артефакта с информацией о запросе/ответе
	ArtifactID int64 `json:"artifact_id,omitempty"`
}

// HTTPData - данные для запроса/ответа
type HTTPData struct {
	// Заголовки запроса/ответа
	Headers []Header `json:"headers,omitempty"`
	// Печеньки запроса/ответа
	Cookies []Cookie `json:"cookies,omitempty"`
	// Тело запроса/ответа
	Body string `json:"body,omitempty"`
}

// Header - данные заголовков
type Header struct {
	// Название заголовка
	Name string `json:"name"`
	// Значения заголовков
	Values []string `json:"values"`
}

// Cookie - данные печенек
type Cookie struct {
	// Название печеньки
	Name string `json:"name"`
	// Значение печеньки
	Value string `json:"value"`
}
