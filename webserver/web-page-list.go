package webserver

import (
	"app/controller"
	"net/http"
	"time"
)

// webPageValue - данные веб страницы
type webPageValue struct {
	// Путь до страницы
	Path string `json:"path"`
	// ИД файла со страницей
	FileID int64 `json:"file_id"`
	// Название страницы
	Name string `json:"name"`
	// Время создания страницы
	Created time.Time `json:"created"`
	// Время последнего обновления страницы
	Updated *time.Time `json:"updated,omitempty"`
}

func webPageFromController(raw *controller.WebPage) webPageValue {
	return webPageValue{
		Path:    raw.Path,
		FileID:  raw.FileID,
		Name:    raw.Name,
		Created: raw.Created,
		Updated: raw.Updated,
	}
}

type webPageListResponse struct {
	// Список веб страниц
	Items []webPageValue `json:"items"`
	// Данные о страницах
	Page pagesInfo `json:"page"`
}

type webPageListRequest struct {
	// Запрашиваемая страница
	Page int64 `json:"page"`
	// Только именованные веб страницы
	OnlyNamed bool `json:"only_named"`
	// Количество элементов на странице
	OnPageCount int64 `json:"on_page_count"`
}

func (ws *WebServer) webPageList() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(webPageListRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		items, info, err := ws.controller.GetWebPagesPage(ctx, req.Page, req.OnPageCount, req.OnlyNamed)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		resp := &webPageListResponse{
			Page: pagesInfoFromController(info),
		}

		for _, item := range items {
			resp.Items = append(resp.Items, webPageFromController(item))
		}

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
