package webserver

import (
	"app/controller"
	"net/http"
	"time"
)

// stateValue - данные состояния
type stateValue struct {
	// Ключ записи
	Key string `json:"key"`
	// Значение записи
	Value string `json:"value"`
	// Время создания записи
	Created time.Time `json:"created"`
	// Время последнего обновления записи
	Updated *time.Time `json:"updated,omitempty"`
}

func stateFromController(raw *controller.State) stateValue {
	return stateValue{
		Key:     raw.Key,
		Value:   raw.Value,
		Created: raw.Created,
		Updated: raw.Updated,
	}
}

type stateListResponse struct {
	// Список состояний
	Items []stateValue `json:"items"`
	// Данные о страницах
	Page pagesInfo `json:"page"`
}

type stateListRequest struct {
	// Запрашиваемая страница
	Page int64 `json:"page"`
	// Количество элементов на странице
	OnPageCount int64 `json:"on_page_count"`
}

func (ws *WebServer) stateList() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(stateListRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		items, info, err := ws.controller.GetStatesPage(ctx, req.Page, req.OnPageCount)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		resp := &stateListResponse{
			Page: pagesInfoFromController(info),
		}

		for _, item := range items {
			resp.Items = append(resp.Items, stateFromController(item))
		}

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
