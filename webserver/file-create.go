package webserver

import (
	"app/controller"
	"net/http"
)

type fileCreateRequest struct {
	// Тело файла
	Body string `json:"body"`
	// Тело файла не закодировано
	BodyIsRaw bool `json:"body_is_raw,omitempty"`
	// Название файла
	Filename string `json:"filename"`
	// MIME тип файла
	Mime string `json:"mime"`
}

type fileCreateResponse struct {
	// Новый Ид
	ID int64 `json:"id"`
}

func (ws *WebServer) fileCreate() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(fileCreateRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		var fileBody []byte

		if req.BodyIsRaw {
			fileBody = []byte(req.Body)
		} else {
			fileBody, err = fromBase64ToRaw(req.Body)
			if err != nil {
				writeJSON(ctx, w, http.StatusBadRequest, err)

				return
			}
		}

		id, err := ws.controller.CreateFile(ctx, controller.File{
			Body:     fileBody,
			Filename: req.Filename,
			Mime:     req.Mime,
		})
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		resp := fileCreateResponse{
			ID: id,
		}

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
