package webserver

import (
	"net/http"
)

type stateGetRequest struct {
	// Ключ записи
	Key string `json:"key"`
}

func (ws *WebServer) stateGet() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(stateGetRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		item, err := ws.controller.GetState(ctx, req.Key)
		if err != nil {
			writeJSON(ctx, w, httpCodeFromError(err), err)

			return
		}

		resp := stateFromController(item)

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
