package webserver

import (
	"app/controller"
	"encoding/base64"
	"net/http"
	"time"
)

// fileValue - данные индексированного значения
type fileValue struct {
	// ИД файла (индекс)
	ID int64 `json:"id"`
	// Тело файла
	Body string `json:"body"`
	// Тело файла не закодировано
	BodyIsRaw bool `json:"body_is_raw"`
	// Название файла
	Filename string `json:"filename"`
	// MIME тип файла
	Mime string `json:"mime"`
	// Размер файла
	Size int64 `json:"size"`
	// Время создания файла
	Created time.Time `json:"created"`
	// Время последнего обновления файла
	Updated *time.Time `json:"updated,omitempty"`
}

func fileFromController(raw *controller.File, bodyIsRaw bool, noBody bool) fileValue {
	var body string

	switch {
	case noBody:
		// Тело не требуется

	case bodyIsRaw:
		body = string(raw.Body)

	default:
		body = fromRawToBase64(raw.Body)

	}

	return fileValue{
		ID:       raw.ID,
		Body:     body,
		Filename: raw.Filename,
		Mime:     raw.Mime,
		Size:     raw.Size,
		Created:  raw.Created,
		Updated:  raw.Updated,
	}
}

func fromRawToBase64(raw []byte) string {
	return base64.StdEncoding.EncodeToString(raw)
}

func fromBase64ToRaw(raw string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(raw)
}

type fileListResponse struct {
	// Список артефактов
	Items []fileValue `json:"items"`
	// Данные о страницах
	Page pagesInfo `json:"page"`
}

type fileListRequest struct {
	// Запрашиваемая страница
	Page int64 `json:"page"`
	// Количество элементов на странице
	OnPageCount int64 `json:"on_page_count"`
}

func (ws *WebServer) fileList() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(fileListRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		items, info, err := ws.controller.GetFilesPage(ctx, req.Page, req.OnPageCount)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		resp := &fileListResponse{
			Page: pagesInfoFromController(info),
		}

		for _, item := range items {
			resp.Items = append(resp.Items, fileFromController(item, false, false))
		}

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
