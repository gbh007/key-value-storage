package webserver

import (
	"net/http"
)

type artifactDeleteRequest struct {
	// ИД записи (индекс)
	ID int64 `json:"id"`
}

func (ws *WebServer) artifactDelete() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(artifactDeleteRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = ws.controller.DeleteArtifact(ctx, req.ID)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		writeNoContent(w)
	})
}
