package webserver

import (
	"app/logger"
	"context"
	"errors"
	"net"
	"net/http"
	"time"
)

// StartServer - инициализирует и запускает веб сервер
func (ws *WebServer) StartServer(ctx context.Context, addr string) <-chan struct{} {
	closeChan := make(chan struct{})

	server := http.Server{
		Addr:         addr,
		Handler:      ws.mux,
		ErrorLog:     logger.StdErrorLogger(ctx),
		BaseContext:  func(l net.Listener) context.Context { return ctx },
		ReadTimeout:  time.Second * 30,
		WriteTimeout: time.Second * 30,
	}

	go func() {
		defer close(closeChan)

		logger.Info(ctx, "Запущен веб сервер")
		defer logger.Info(ctx, "Веб сервер остановлен")

		err := server.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Error(ctx, err)
		}

	}()

	go func() {
		<-ctx.Done()
		logger.Info(ctx, "Остановка веб сервера")

		shutdownCtx, cancel := context.WithTimeout(context.TODO(), time.Second*10)
		defer cancel()

		logger.IfErr(ctx, server.Shutdown(shutdownCtx))
	}()

	return closeChan
}
