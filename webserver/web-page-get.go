package webserver

import (
	"net/http"
)

type webPageGetRequest struct {
	// Путь до веб страницы
	Path string `json:"path"`
}

func (ws *WebServer) webPageGet() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(webPageGetRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		item, err := ws.controller.GetWebPage(ctx, req.Path)
		if err != nil {
			writeJSON(ctx, w, httpCodeFromError(err), err)

			return
		}

		resp := webPageFromController(item)

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
