package webserver

import (
	"net/http"
)

type artifactUpdateRequest struct {
	// ИД записи (индекс)
	ID int64 `json:"id"`
	// Значение записи
	Value string `json:"value"`
}

func (ws *WebServer) artifactUpdate() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(artifactUpdateRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}
		err = ws.controller.UpdateArtifact(ctx, req.ID, req.Value)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		writeNoContent(w)
	})
}
