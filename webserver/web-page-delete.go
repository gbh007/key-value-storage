package webserver

import (
	"net/http"
)

type webPageDeleteRequest struct {
	// Путь до страницы
	Path string `json:"path"`
}

func (ws *WebServer) webPageDelete() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(webPageDeleteRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = ws.controller.DeleteWebPage(ctx, req.Path)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		writeNoContent(w)
	})
}
