package webserver

import (
	"app/controller"
	"app/logger"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
)

type WebServer struct {
	mux        *http.ServeMux
	controller *controller.Controller
}

func Init(cntl *controller.Controller) *WebServer {

	mux := http.NewServeMux()
	ws := &WebServer{
		mux:        mux,
		controller: cntl,
	}

	mux.Handle("/", getWrapper(ws.rootHandler()))

	mux.Handle("/api/artifact/list", postWrapper(ws.artifactList()))
	mux.Handle("/api/artifact/create", postWrapper(ws.artifactCreate()))
	mux.Handle("/api/artifact/get", postWrapper(ws.artifactGet()))
	mux.Handle("/api/artifact/update", postWrapper(ws.artifactUpdate()))
	mux.Handle("/api/artifact/delete", postWrapper(ws.artifactDelete()))

	mux.Handle("/api/state/list", postWrapper(ws.stateList()))
	mux.Handle("/api/state/set", postWrapper(ws.stateSet()))
	mux.Handle("/api/state/get", postWrapper(ws.stateGet()))
	mux.Handle("/api/state/delete", postWrapper(ws.stateDelete()))

	mux.Handle("/file", getWrapper(ws.fileGetRaw()))
	mux.Handle("/api/file/list", postWrapper(ws.fileList()))
	mux.Handle("/api/file/create", postWrapper(ws.fileCreate()))
	mux.Handle("/api/file/get", postWrapper(ws.fileGet()))
	mux.Handle("/api/file/update", postWrapper(ws.fileUpdate()))
	mux.Handle("/api/file/delete", postWrapper(ws.fileDelete()))

	mux.Handle("/api/web-page/list", postWrapper(ws.webPageList()))
	mux.Handle("/api/web-page/set", postWrapper(ws.webPageSet()))
	mux.Handle("/api/web-page/get", postWrapper(ws.webPageGet()))
	mux.Handle("/api/web-page/delete", postWrapper(ws.webPageDelete()))

	mux.Handle("/api/requester", postWrapper(ws.requester()))

	return ws
}

func postWrapper(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			p := recover()
			if p != nil {
				logger.Warning(r.Context(), "panic found", r.URL.String(), p)
			}
		}()

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")

		switch r.Method {

		case http.MethodOptions:
			w.WriteHeader(http.StatusNoContent)

			return

		case http.MethodPost:
			next.ServeHTTP(w, r)

		default:
			w.WriteHeader(http.StatusMethodNotAllowed)

			return
		}

	})
}

func getWrapper(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			p := recover()
			if p != nil {
				logger.Warning(r.Context(), "panic found", r.URL.String(), p)
			}
		}()

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")

		switch r.Method {

		case http.MethodOptions:
			w.WriteHeader(http.StatusNoContent)

			return

		case http.MethodGet:
			next.ServeHTTP(w, r)

		default:
			w.WriteHeader(http.StatusMethodNotAllowed)

			return
		}

	})
}

// parseJSON - парсит тело запроса в переданный объект
func parseJSON(ctx context.Context, r *http.Request, data interface{}) error {
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		logger.Debug(ctx, err)
	}

	return err
}

// writeJSON - записывает ответ в формате JSON
func writeJSON(ctx context.Context, w http.ResponseWriter, statusCode int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	enc := json.NewEncoder(w)
	enc.SetIndent("", "  ")

	if errData, ok := data.(error); ok {
		data = errData.Error()
	}

	if err := enc.Encode(data); err != nil {
		logger.Error(ctx, err)
	}

}

// writeNoContent - записывает ответ без тела
func writeNoContent(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

func httpCodeFromError(err error) int {
	switch {
	case errors.Is(err, sql.ErrNoRows):
		return http.StatusNotFound
	}

	return http.StatusInternalServerError
}
