package webserver

import (
	"net/http"
)

type artifactCreateRequest struct {
	// Новые данные
	Value string `json:"value"`
}

type artifactCreateResponse struct {
	// Новый Ид
	ID int64 `json:"id"`
}

func (ws *WebServer) artifactCreate() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(artifactCreateRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		id, err := ws.controller.CreateArtifact(ctx, req.Value)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		resp := artifactCreateResponse{
			ID: id,
		}

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
