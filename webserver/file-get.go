package webserver

import (
	"net/http"
	"strconv"
)

type fileGetRequest struct {
	// ИД записи (индекс)
	ID int64 `json:"id"`
	// Не кодировать тело файла
	BodyIsRaw bool `json:"body_is_raw,omitempty"`
	// Не отдавать тело файла
	NoBody bool `json:"no_body,omitempty"`
}

func (ws *WebServer) fileGet() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(fileGetRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		item, err := ws.controller.GetFile(ctx, req.ID)
		if err != nil {
			writeJSON(ctx, w, httpCodeFromError(err), err)

			return
		}

		resp := fileFromController(item, req.BodyIsRaw, req.NoBody)

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}

func (ws *WebServer) fileGetRaw() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		fileID, err := strconv.ParseInt(r.URL.Query().Get("id"), 10, 64)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		item, err := ws.controller.GetFile(ctx, fileID)
		if err != nil {
			writeJSON(ctx, w, httpCodeFromError(err), err)

			return
		}

		w.Header().Set("Content-Type", item.Mime)
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(item.Body)
	})
}
