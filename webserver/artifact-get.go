package webserver

import (
	"net/http"
)

type artifactGetRequest struct {
	// ИД записи (индекс)
	ID int64 `json:"id"`
}

func (ws *WebServer) artifactGet() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(artifactGetRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		item, err := ws.controller.GetArtifact(ctx, req.ID)
		if err != nil {
			writeJSON(ctx, w, httpCodeFromError(err), err)

			return
		}

		resp := artifactFromController(item)

		writeJSON(ctx, w, http.StatusOK, resp)
	})
}
