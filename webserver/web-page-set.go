package webserver

import (
	"net/http"
)

type webPageSetRequest struct {
	// Прошлый путь веб страницы
	OldPath string `json:"old_path"`
	// Путь веб страницы
	Path string `json:"path"`
	// ID файла веб страницы
	FileID int64 `json:"file_id"`
	// Название страницы
	Name string `json:"name,omitempty"`
}

func (ws *WebServer) webPageSet() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(webPageSetRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		if req.OldPath == "" {
			req.OldPath = req.Path
		}

		err = ws.controller.SetWebPage(ctx, req.OldPath, req.Path, req.FileID, req.Name)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		writeNoContent(w)
	})
}
