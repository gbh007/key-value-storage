package webserver

import (
	"app/controller"
	"net/http"
)

type fileUpdateRequest struct {
	// ИД файла
	ID int64 `json:"id"`
	// Тело файла
	Body string `json:"body"`
	// Тело файла не закодировано
	BodyIsRaw bool `json:"body_is_raw,omitempty"`
	// Название файла
	Filename string `json:"filename"`
	// MIME тип файла
	Mime string `json:"mime"`
}

func (ws *WebServer) fileUpdate() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(fileUpdateRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		var fileBody []byte

		if req.BodyIsRaw {
			fileBody = []byte(req.Body)
		} else {
			fileBody, err = fromBase64ToRaw(req.Body)
			if err != nil {
				writeJSON(ctx, w, http.StatusBadRequest, err)

				return
			}
		}

		err = ws.controller.UpdateFile(ctx, controller.File{
			ID:       req.ID,
			Body:     fileBody,
			Filename: req.Filename,
			Mime:     req.Mime,
		})
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		writeNoContent(w)
	})
}
