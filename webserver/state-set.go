package webserver

import (
	"net/http"
)

type stateUpdateRequest struct {
	// Прошлый ключ записи
	OldKey string `json:"old_key,omitempty"`
	// Ключ записи
	Key string `json:"key"`
	// Значение записи
	Value string `json:"value"`
}

func (ws *WebServer) stateSet() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		req := new(stateUpdateRequest)

		err := parseJSON(ctx, r, &req)
		if err != nil {
			writeJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		if req.OldKey == "" {
			req.OldKey = req.Key
		}

		err = ws.controller.SetState(ctx, req.OldKey, req.Key, req.Value)
		if err != nil {
			writeJSON(ctx, w, http.StatusInternalServerError, err)

			return
		}

		writeNoContent(w)
	})
}
