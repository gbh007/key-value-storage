TAG = $(shell git tag -l --points-at HEAD)
COMMIT = $(shell git show -s --abbrev=12 --pretty=format:%h HEAD)
BUILD_TIME = $(shell date +"%Y-%m-%d %H:%M:%S")

LDFLAGS = -ldflags "-linkmode external -extldflags -static -X 'app/controller.Version=$(TAG)' -X 'app/controller.Commit=$(COMMIT)' -X 'app/controller.BuildAt=$(BUILD_TIME)'"

create_build_dir:
	mkdir -p ./_build


build_arm64: create_build_dir
	CC=/usr/bin/aarch64-linux-gnu-gcc CGO_ENABLED=1 GOOS=linux GOARCH=arm64 go build $(LDFLAGS) -v -o ./_build/kvs-linux-arm64

build_linux_amd64: create_build_dir
	CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build  $(LDFLAGS) -o ./_build/kvs-linux-amd64

build_windows_amd64: create_build_dir
	CC=/usr/bin/x86_64-w64-mingw32-gcc  CGO_ENABLED=1 GOOS=windows GOARCH=amd64 go build  $(LDFLAGS)  -o ./_build/kvs-windows-amd64.exe

build: build_arm64 build_linux_amd64 build_windows_amd64

run: create_build_dir
	go build  $(LDFLAGS) -o ./_build/kvs-bin
	./_build/kvs-bin -addr 127.0.0.1:8081 -d